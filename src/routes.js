import Home from './components/Home.vue'
import Clenove from './components/Clenove.vue'
import Prihlaska from './components/Prihlaska.vue'

export default [
	{ path: '/', component: Home},	
	{ path: '/clenove', component: Clenove},
	{ path: '/prihlaska', component: Prihlaska}
]